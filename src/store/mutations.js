export default {
  setState(state, proxies) {
    if (proxies) {
      state.proxies = proxies;
    }
  }
};

import { data } from "../api/proxies";
const url = "https://proxyfordevelopers.com/api/proxies/?format=json";
/**
 * this action is trying to fetch data from the api
 * but it will not work actually because of the cors
 */
export default {
  fetchProxies({ commit }) {
    const req = new Request(url, { method: "GET", mode: "no-cors" });
    fetch(req)
      .then(response => response.json())
      .then(data => commit("setState", data))
      .catch(err => {
        console.log(
          `fail to fetch data initializing with fake data: ${
            data.length
          } ${err}`
        );
        commit("setState", data);
      });
  }
};

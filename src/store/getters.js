export default {
  getAllProxies: state => {
    return state.proxies.slice();
  },

  getByType: type => state => {
    state.proxies.filter(proxy => proxy.type === type);
  },

  getByCountry: country => state => {
    state.proxies.filter(proxy => proxy.country === country);
  }
};

import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import ProxyDisplay from "@/views/ProxyDisplay.vue";
import ProxyList from "@/components/proxy/ProxyList.vue";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("ProxyDisplay.vue", () => {
  let store;
  let getters;
  beforeEach(() => {
    getters = {
      getAllProxies: () => []
    };

    store = new Vuex.Store({
      getters
    });
  });
  test("should render the proxy list component", () => {
    const wrapper = shallowMount(ProxyDisplay, { store, localVue });

    expect(wrapper.contains(ProxyList)).toBeTruthy();

    const Proxies = wrapper.find(ProxyList);
    expect(Proxies.isVueInstance()).toBeTruthy();
  });

  test("should contains a list of checkbox and a select button", () => {
    const wrapper = shallowMount(ProxyDisplay, { store, localVue });
    const checkboxes = wrapper.findAll("input[type='checkbox']");
    expect(checkboxes.length).toBeGreaterThan(1);

    const aliveCheckox = wrapper.find("input#alive");
    aliveCheckox.setChecked(true);
    aliveCheckox.trigger("change");
    expect(wrapper.vm.alive).toBe(true);
  });
});

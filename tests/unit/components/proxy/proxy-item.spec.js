import { shallowMount } from "@vue/test-utils";
import ProxyItem from "@/components/proxy/ProxyItem.vue";
const proxy = {
  id: 1,
  country: "RU",
  port: 8080
};

const factory = () => {
  return shallowMount(ProxyItem, {
    propsData: { proxy }
  });
};

describe("ProxyItem.vue", () => {
  test("should render a proxy component", () => {
    const wrapper = factory();
    expect(wrapper.isVueInstance()).toBe(true);
  });

  test("should render props correctly", () => {
    const wrapper = factory();
    expect(wrapper.text()).toContain(proxy.country);
    expect(wrapper.text()).toContain(proxy.port);
  });
});

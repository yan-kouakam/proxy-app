import { shallowMount } from "@vue/test-utils";
import ProxyList from "@/components/proxy/ProxyList.vue";
import ProxyItem from "@/components/proxy/ProxyItem.vue";

describe("ProxyList.vue", () => {
  test("should render a list off proxy items", () => {
    const proxies = [
      { id: 1, country: "FR", port: 8080 },
      { id: 3, country: "GB", port: 3000 },
      { id: 4, country: "RU", port: 9000 }
    ];
    const wrapper = shallowMount(ProxyList, {
      propsData: { proxies }
    });
    const Items = wrapper.findAll(ProxyItem);
    expect(Items).toHaveLength(proxies.length);

    Items.wrappers.forEach((wrapper, ind) => {
      expect(wrapper.vm.proxy).toEqual(proxies[ind]);
    });
  });
});

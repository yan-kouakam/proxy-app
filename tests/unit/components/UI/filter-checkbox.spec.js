import { shallowMount } from "@vue/test-utils";
import CheckBoxFilter from "@/components/UI/CheckBoxFilter.vue";

describe("CheckBoxFilter.vue", () => {
  test("should contains a checkbox input witch is initially unchecked", () => {
    const props = {
      name: "a name",
      value: "val",
      id: "test"
    };
    const wrapper = shallowMount(CheckBoxFilter, {
      propsData: { ...props }
    });

    expect(wrapper.contains("input[type='checkbox']")).toBeTruthy();

    const input = wrapper.find("input");
    expect(input.attributes()).not.toContain("checked");
  });
});
